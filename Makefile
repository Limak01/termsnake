termsnake: src
	mkdir -p build
	$(CC) src/*.c src/*.h -o build/termsnake -Wall -Wextra -pedantic -std=c99 -D_POSIX_C_SOURCE=199309L
