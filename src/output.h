#ifndef OUTPUT_H
#define OUTPUT_H
    
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "snake.h"

#define CLEAR_SCREEN "\x1b[2J"
#define RESET_CURSOR "\x1b[H"
#define INIT_BUFF {NULL, 0}

typedef struct {
    char* str;
    int len;
} Buff;

void refresh_screen();

#endif
