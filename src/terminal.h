#ifndef TERM_H
#define TERM_H

#include <termios.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/ioctl.h>

#include "snake.h"

void disable_raw_mode();
void into_raw_mode();
int get_terminal_size(int* width, int* height);

#endif
