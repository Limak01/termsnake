#include <stdio.h>
#include <unistd.h>
#include <time.h>

#include "terminal.h"
#include "snake.h"
#include "input.h"
#include "output.h"

int main(int argc, char** argv) {
    srand(time(NULL));
    into_raw_mode();
    init_snake();

    while(1) {
        struct timespec req, rem;

    // Ustaw czas na 0.7 sekundy
    req.tv_sec = 0;            // 0 sekund
    req.tv_nsec = 70000000;
        nanosleep(&req, &rem);
        refresh_screen();
        process_key_press(); 
    }

    return 0;
}
