#ifndef SNAKE_H
#define SNAKE_H

#include <stdlib.h>

#include "terminal.h"


struct Pos {
    int x;
    int y;
};

typedef struct {
    int body_len;
    int body_capacity;
    struct Pos* body;
    int screen_w, screen_h;
    struct Pos point;
    int score;
    // Snake current direction
    struct Pos dir;
} Snake;

extern Snake snake;
void init_snake(); 
void snake_move();
void change_dir(struct Pos dir);
void add_tail();
void collect_point();


#endif
