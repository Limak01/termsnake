#include "snake.h"

Snake snake;

void init_snake() {
    snake.score = 0;
    snake.body_len = 3;
    snake.body_capacity = 5;
    snake.dir = (struct Pos){1, 0};
    snake.body = malloc(sizeof(struct Pos) * snake.body_capacity); 
    snake.body[0] = (struct Pos){4, 2};
    snake.body[1] = (struct Pos){4, 3};
    snake.body[2] = (struct Pos){4, 4};
    
    if(get_terminal_size(&snake.screen_w, &snake.screen_h) == -1) {
        perror("Getting terminal size");
        exit(1);
    }

    snake.point.x = rand() % snake.screen_w;
    snake.point.y = rand() % snake.screen_h;
    
    // Make space for status bar
    snake.screen_h--;
}

void add_tail() {
    struct Pos new;
    
    new.x = snake.body[snake.body_len - 1].x;
    new.y = snake.body[snake.body_len - 1].y;

    snake.body[snake.body_len] = new;
    snake.body_len++;

    if(snake.body_len == snake.body_capacity) {
        snake.body_capacity += 5;
        snake.body = realloc(snake.body, sizeof(struct Pos) * snake.body_capacity);
    }

}

void collect_point() {
    snake.score += 5;

    snake.point.x = rand() % snake.screen_w;
    snake.point.y = rand() % snake.screen_h;

    add_tail();
}

void change_dir(struct Pos dir) {
    if(snake.dir.x + dir.x != snake.body[1].x && snake.dir.y + dir.y != snake.body[1].y) {
        snake.dir.x = dir.x;
        snake.dir.y = dir.y;
    }
}

void snake_move() {
    for(int i = snake.body_len; i >= 1; i--) {
        snake.body[i].x = snake.body[i - 1].x;
        snake.body[i].y = snake.body[i - 1].y;
    }

    snake.body[0].x += snake.dir.x;
    snake.body[0].y += snake.dir.y;

    if(snake.body[0].x <= 0) snake.body[0].x = snake.screen_w;
    if(snake.body[0].x > snake.screen_w) snake.body[0].x = 0;
    if(snake.body[0].y <= 0) snake.body[0].y = snake.screen_h;
    if(snake.body[0].y > snake.screen_h) snake.body[0].y = 0;

    if(snake.body[0].x == snake.point.x && snake.body[0].y == snake.point.y) {
        collect_point();
    }

    for(int i = 1; i < snake.body_len; i++) {
        if(snake.body[0].x == snake.body[i].x && snake.body[0].y == snake.body[i].y) {
            exit(0);
        }
    }
}
