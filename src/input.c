#include "input.h"

int read_key() {
    int c;

    while(read(STDIN_FILENO, &c, 1) == 1);

    return c;
}

void process_key_press() {
    char key = read_key();

    switch(key) {
        case 'q':
            exit(0);
        break;
        case 'w':
            change_dir((struct Pos){0, -1});
        break;
        case 'a':
            change_dir((struct Pos){-1, 0});
        break;
        case 'd':
            change_dir((struct Pos){1, 0});
        break;
        case 's':
            change_dir((struct Pos){0, 1});
        break;
    }
}
