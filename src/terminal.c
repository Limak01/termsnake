#include "terminal.h"

// Original terminal settings
struct termios term;

void disasble_raw_mode() {
    if(tcsetattr(STDIN_FILENO, TCSAFLUSH, &term) == -1) return;
    printf("You finished with %d points \n", snake.score);
}

void into_raw_mode() { 
    if(tcgetattr(STDIN_FILENO, &term) == -1) {
        perror("Enabling raw mode");
        exit(1);
    }
    
    atexit(disasble_raw_mode);

    struct termios raw_term = term;
    
    raw_term.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    raw_term.c_oflag &= ~(OPOST);
    raw_term.c_cflag |= (CS8);
    raw_term.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
    raw_term.c_cc[VMIN] = 0;
    raw_term.c_cc[VTIME] = 0;

    if(tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw_term) == -1) {
        perror("Enabling raw mode");
        exit(1);
    }
}

int get_terminal_size(int* width, int* height) {
    struct winsize win;
    
    if(ioctl(STDOUT_FILENO, TIOCGWINSZ, &win) == -1) return -1;
    
    *width = win.ws_col;
    *height = win.ws_row;

    return 1;
}
