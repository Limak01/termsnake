#include "output.h"

int b_append(Buff* buff, char* s, int size) {
    char* new = realloc(buff->str, buff->len + size);

    if(new == NULL) return -1;

    memcpy(&new[buff->len], s, size);
    buff->len += size;
    buff->str = new;

    return size;
}

void b_free(Buff* buff) {
    free(buff->str);
}

void draw_status_bar(Buff* buff) {
    char buf[100];
    int b_len = sprintf(buf, "\x1b[7m\x1b[%d;0H", snake.screen_h + 1);
    b_append(buff, buf, b_len); 

    int len = sprintf(buf, "Pos (x: %d, y: %d)  Score: %d", snake.body[0].x, snake.body[0].y, snake.score);
    b_append(buff, buf, len);

    for(int i = 0; i < snake.screen_w - len; i++) {
        b_append(buff, " ", 1);
    }
    b_append(buff, "\x1b[0m", 4);
}

void draw(Buff* buff) {
    
    // Draw snake
    for(int i = 0; i < snake.body_len; i++) {
        char cursor[16];
        sprintf(cursor, "\x1b[%d;%dH", snake.body[i].y, snake.body[i].x); 
        b_append(buff, cursor, strlen(cursor));
        b_append(buff, "\x1b[42m \x1b[0m", 10);
    }

    // Draw point
    char buf[30];
    int len = sprintf(buf, "\x1b[41m\x1b[%d;%dH \x1b[0m", snake.point.y, snake.point.x);
    b_append(buff, buf, len);
}

void refresh_screen() {
    Buff buff = INIT_BUFF;
    fflush(stdout);

    b_append(&buff, CLEAR_SCREEN, 4);
    b_append(&buff, RESET_CURSOR, 3);

    snake_move(); 
    draw(&buff);
    draw_status_bar(&buff);

    b_append(&buff, RESET_CURSOR, 3);
    write(STDOUT_FILENO, buff.str, buff.len);
    
    b_free(&buff);
}
