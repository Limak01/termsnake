#ifndef INPUT_H
#define INPUT_H

#include <unistd.h>
#include <stdlib.h>

#include "snake.h"

int read_key();
void process_key_press();

#endif
